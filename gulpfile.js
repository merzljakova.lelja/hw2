const { parallel, series } = require("gulp");
const { scripts } = require("./gulp-tasks/scripts.js");
const { styles } = require("./gulp-tasks/styles.js");
const { images } = require("./gulp-tasks/images.js");
const { serv } = require("./gulp-tasks/serv.js");
const { watcher } = require("./gulp-tasks/watcher.js");
const { cleaner } = require('./gulp-tasks/cleaner.js');



exports.build = parallel(serv,watcher, cleaner, series(images, styles, scripts));

exports.dev = parallel (serv, watcher, series( styles, scripts))
