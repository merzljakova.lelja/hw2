
"use strict"   
function burgerMenu(selector) {
    let menu = $(selector);
    let button = menu.find('.burger-menu_button', '.burger-menu_lines');  
    button.on('click', (e) => {
      e.preventDefault();
      toggleMenu();
    });    
    function toggleMenu(){
      menu.toggleClass('burger-menu_active');
    }
   }
  
  burgerMenu('.burger-menu');

