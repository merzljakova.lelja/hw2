const { src, dest } = require("gulp");
const { bs } = require("./serv.js");
const sass = require('gulp-sass')(require('sass'));
const sourcemaps = require("gulp-sourcemaps");
const cleanCSS = require('gulp-clean-css');
const autoPrefix = require('gulp-autoprefixer');
const concat = require('gulp-concat');

function styles() {
   return src("./src/styles/**/*.scss")
      .pipe(sourcemaps.init())
      .pipe(sass().on('error', sass.logError))
      .pipe(autoPrefix('last 8 versions'))
      .pipe(sourcemaps.write())
      .pipe(concat('styles.min.css'))
      .pipe(cleanCSS({compatibility: 'ie8'}))
      .pipe(dest("./dist/css"))
      .pipe(bs.reload({ stream: true }));
}

exports.styles = styles;

