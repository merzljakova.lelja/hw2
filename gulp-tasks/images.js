const { src, dest } = require("gulp");
const { bs } = require("./serv.js");
const imgMin = require('gulp-imagemin');

function images() {
   return src("./src/images/**/*.{jpg,jpeg,png,gif,tiff,svg}")
      .pipe(imgMin())
      .pipe(dest("./dist/img"))
      .on("end", bs.reload);
}

exports.images = images;
